﻿namespace ProgramArguments {

	public class Program {
		
		public static void Main(string[] args) {
				System.Console.WriteLine(args.Length + " Arguments:");

				for (int i = 0; i < args.Length; ++i) {
					System.Console.WriteLine("\t" + args[i]);
				}

				System.Environment.Exit(0);
		}
	}
}
